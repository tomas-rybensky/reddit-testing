*** Variables ***
${comment_tab}    xpath=//a[.="Comments"]
${delete_comment_button}    xpath=//button[.//span = 'delete']
${delete_comment_popup_button}    xpath=//button[.= 'delete']
${user_existing_comment}    xpath=//a[.//h2 = 'Title']
${delete_comment_success}    Comment deleted