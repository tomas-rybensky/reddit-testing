* Variables *
${title_noneditable}    xpath=//div[@class='_1rcejqgj_laTEewtp2DbWG s1knm1ot-0 cKmWbx']//span//h2
${save_post_button}    //button[@class='s1434uai-4 bRCHUJ']
${edit_post_button}    xpath=//button[.//span = 'Edit post']
${edit_post_detail_button}    xpath=(//button[.//span = 'Edit post'])[2]
${edit_post_success}    Post successfully edited
${editor}         //div[@class='DraftEditor-editorContainer']
${switch-markdown}    //div[@class='s16eky3e-0 dzQBnz']/div[.//span = 'Switch to markdown']
