*** Variables ***
${delete_post_success}    Post deleted successfully
${delete_post_button}    xpath=//button[.//span = 'delete']
${delete_post_popup_button}    xpath=//button[.= 'delete post']
${more_options_button}    xpath=//button[.//i/@class= 'icon icon-menu mpc6lx-2 ebwjqI']
${posts_button}    xpath=//a[.="Posts"]
${close-cookie-panel}      //*[@id="SHORTCUT_FOCUSABLE_DIV"]/div[2]/form/div/button
${test_comment_text}       Great!
${comment_button}          //a[@data-test-id="comments-page-link-num-comments"][1]
${comment_input}           //*[@id="SHORTCUT_FOCUSABLE_DIV"]/div/div/div/div[2]/div[2]/div[1]/div/div[2]/div[2]/div/div/div[2]/div/div[1]/div/div/div
${confirm_comment}         //*[@id="SHORTCUT_FOCUSABLE_DIV"]/div/div/div/div[2]/div[2]/div[1]/div/div[2]/div[2]/div/div/div[3]/div[1]
