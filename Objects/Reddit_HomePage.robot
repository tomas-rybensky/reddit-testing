*** Variables ***
${reddit_title}    reddit: the front page of the internet
${reddit_log_in_button}    log in
${reddit_log_in_title}    reddit.com: Log in
${reddit_log_in_iframe}    //iframe[@src='https://www.reddit.com/login']
${reddit_login_username_input}    //input[@id='loginUsername']
${reddit_login_password_input}    //input[@id='loginPassword']
${reddit_login_button_submit}    //button[@type='submit']
${reddit_logged_in}    User account menu
${reddit_login_fail}    Incorrect username or password
