*** Variables ***
${new_post_button}    id=HeaderUserActions--NewPost
${test_title}     Title
${test_text}      Text
${title_input}    //textarea
${text_input}     //*[@placeholder='Text (optional)']
${change_to_markdown}    //*[@id="SHORTCUT_FOCUSABLE_DIV"]/div/div/div/div[2]/div[2]/div[1]/div/div[3]/div[2]/div[2]/div/div/div[1]/div[19]
${community_input}    //*[@placeholder='Choose a community']
${submit_post_button}    //*[@id="SHORTCUT_FOCUSABLE_DIV"]/div/div/div/div[2]/div[2]/div[1]/div/div[3]/div[3]/div[2]/div/div[1]/button
${posted}         Posted
