*** Variables ***
${delete_post_success}    Post deleted successfully
${delete_post_button}    xpath=//button[.//span = 'delete']
${delete_post_popup_button}    xpath=//button[.= 'delete post']
${more_options_button}    xpath=//button[.//i/@class= 'icon icon-menu mpc6lx-2 ebwjqI']
${posts_button}    xpath=//a[.="Posts"]
