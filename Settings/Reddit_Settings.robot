*** Variables ***
${url}            https://reddit.com/
${browser}        chrome
${reddit_username}    redditTest45
${reddit_password}    password
${reddit_username_invalid}    redditTest46
${reddit_password_invalid}    wrongpassword
${community_value}    r/4IT446test
${url_user}       https://reddit.com/u/redditTest45
${url_different_users_post}    https://www.reddit.com/r/4IT446test/comments/9w5y70/test_post_that_other_users_cant_edit/
${user_submit}    xpath=//a[@href="/user/redditTest45/submit"]
${user_existing_post}    xpath=//a[.//h2 = 'Title']
