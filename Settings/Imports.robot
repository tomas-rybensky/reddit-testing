*** Settings ***
Library           Selenium2Library
Resource          ../Keywords/Reddit_Keywords.robot
Resource          ../Settings/Reddit_Settings.robot
Resource          ../Objects/Reddit_HomePage.robot
Resource          ../Objects/Reddit_NewPost.robot
Resource          ../Objects/Reddit_EditPost.robot
Resource          ../Objects/Reddit_DeletePost.robot
Resource          ../Objects/Reddit_NewComment.robot
Resource          ../Objects/Reddit_DeleteComment.robot
Resource          ../Objects/Reddit_Logout.robot
