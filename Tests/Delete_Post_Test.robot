*** Settings ***
Test Teardown     Close Reddit
Resource          ../Settings/Imports.robot

*** Test Cases ***
Delete Post Valid
    Open Reddit    ${url_user}    ${browser}
    Login Into Reddit    ${reddit_username}    ${reddit_password}
    Wait Until Page Contains Element    ${user_existing_post}    10
    Delete First Post
    Wait Until Page Contains    ${delete_post_success}    10

Delete Post Of Another User
    Open Reddit    ${url_different_users_post}    ${browser}
    Login Into Reddit    ${reddit_username}    ${reddit_password}
    Wait Until Page Contains    ${community_value}
    Page Should Not Contain Element    ${delete_post_button}
    Click Button    ${more_options_button}
    Page Should Not Contain Element    ${delete_post_button}
