*** Settings ***
Test Setup        Open Reddit    ${url}    ${browser}
Test Teardown     Close Reddit
Resource          ../Settings/Imports.robot

*** Test Cases ***
Create Post Valid
    Login Into Reddit    ${reddit_username}    ${reddit_password}
    Create Post    ${test_title}    ${test_text}
    Wait Until Page Contains    ${posted}
