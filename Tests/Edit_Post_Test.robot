*** Settings ***
Test Teardown     Close Reddit
Resource          ../Settings/Imports.robot

*** Test Cases ***
Edit Post Valid
    Open Reddit    ${url_user}    ${browser}
    Login Into Reddit    ${reddit_username}    ${reddit_password}
    Wait Until Page Contains Element    ${user_existing_post}    10
    Edit Post    ${test_text}
    Wait Until Page Contains    ${edit_post_success}    10

Edit Post Of Another User
    Open Reddit    ${url_different_users_post}    ${browser}
    Login Into Reddit    ${reddit_username}    ${reddit_password}
    Wait Until Page Contains    ${community_value}    10
    Page Should Not Contain Element    ${edit_post_button}
