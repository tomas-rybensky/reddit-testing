*** Settings ***
Test Setup        Open Reddit    ${url}    ${browser}
Test Teardown     Close Reddit
Resource          ../Settings/Imports.robot

*** Test Cases ***
Loging out from Reddit
    Login Into Reddit    ${reddit_username}    ${reddit_password}
    Logout From Reddit
