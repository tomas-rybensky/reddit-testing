*** Settings ***
Resource    ../Settings/Imports.robot
Test Setup    Open Reddit    ${url}    ${browser}
Test Teardown    Close Reddit

*** Test Cases ***
Valid Login
    Login Into Reddit    ${reddit_username}    ${reddit_password}
    Wait Until Page Contains    ${reddit_logged_in}

Invalid Login
    Login Into Reddit    ${reddit_username_invalid}    ${reddit_password_invalid}
    Wait Until Page Contains    ${reddit_login_fail}
