*** Keywords ***
Open Reddit
    [Arguments]    ${url}    ${browser}
    Open Browser    ${url}    ${browser}
    #Maximize Browser Window
    # -- chrome driver on mac cant do that in the last version
    #Click Element    ${close-cookie-panel}

Close Reddit
    #Capture Page Screenshot
    Close Browser

Login Into Reddit
    [Arguments]    ${username}    ${password}
    #Title Should Be    ${reddit_title}
    Click Link    ${reddit_log_in_button}
    Wait Until Page Contains Element    ${reddit_log_in_iframe}
    Select Frame    ${reddit_log_in_iframe}
    Input Text    ${reddit_login_username_input}    ${username}
    Input Text    ${reddit_login_password_input}    ${password}
    Click Button    ${reddit_login_button_submit}

Create Post
    [Arguments]    ${title}    ${text}
    Wait Until Page Contains Element    ${new_post_button}
    Click Element    ${new_post_button}
    Wait Until Page Contains Element    ${title_input}
    Click Element    ${change_to_markdown}
    Input Text    ${community_input}    ${community_value}
    Input Text    ${title_input}    ${title}
    Input Text    ${text_input}    ${text}
    Sleep    10s
    Click Element    ${submit_post_button}

Edit Post
    [Arguments]    ${edited_text}
    Click Button    ${edit_post_button}
    Reload Page
    Wait Until Page Contains Element    ${more_options_button}    10
    Click Button    ${more_options_button}
    Wait Until Element is Visible    ${edit_post_detail_button}    30
    Click Button    ${edit_post_detail_button}
    Wait Until Page Contains Element    ${switch-markdown}    10
    Click Element    ${switch-markdown}
    Wait Until Page Contains Element    ${text_input}    10
    Input Text    ${text_input}    ${edited_text}
    #poz_9: We check whether the Title div contains a h2 tag instead of a TextArea
    Page Should Contain Element    ${title_noneditable}
    Click Button    ${save_post_button}


Delete First Post
    Wait Until Page Contains Element    ${posts_button}    10
    Click Element    ${posts_button}
    Wait Until Page Contains Element    ${more_options_button}    10
    Click Button    ${more_options_button}
    Wait Until Page Contains Element    ${delete_post_button}    10
    Click Button    ${delete_post_button}
    Wait Until Page Contains Element    ${delete_post_popup_button}    10
    Click Button    ${delete_post_popup_button}

Add Koment
    [Arguments]    ${text}
    Wait Until Page Contains Element    ${comment_button}
    Click Element    ${comment_button}
    Reload Page
    Click Element    ${close-cookie-panel}
    Wait Until Page Contains Element    ${comment_input}
    Input Text    ${comment_input}    ${test_comment_text}
    Scroll Element Into View    ${confirm_comment}
    Click Element    ${confirm_comment}

Delete Comment
    Wait Until Page Contains Element    ${comment_tab}    10
    Click Element    ${comment_tab}
    Wait Until Page Contains Element    ${more_options_button}    10
    Click Button    ${more_options_button}
    Wait Until Page Contains Element    ${delete_comment_button}    10
    Click Button    ${delete_comment_button}
    Wait Until Page Contains Element    ${delete_comment_popup_button} 10
    Click Button    ${delete_comment_popup_button}
    Wait Until Page Contains Element	${delete_comment_success}	10

Logout From Reddit
    Wait Until Page Contains Element     ${reddit_open_menu_button}
    Click Element    ${reddit_open_menu_button}
    Wait Until Page Contains Element     ${reddit_log_out_button}
    Click Element    ${reddit_log_out_button}
    Wait Until Page Contains    ${logout_success}